# Isaac Anderson
# CS 1400-1
# Assn 13 Task 2
# Program to check a password.

# Import statements
from password import Password


def main():
    play = True

    while play:
        password = Password(input("Enter a password to see if it is strong: ").lower())
        print(password.isValid())

        play = True if input("Do you want to play again (Y/N)? ").lower() == 'y' else False


main()
