# Isaac Anderson
# CS 1400-1
# Assn 13 Task 1
# Program to draw a chessboard with user input.


class Chessboard:
    def __init__(self, tr, boardX, boardY, height=250, width=250):
        self.__tr = tr
        self.__height = height
        self.__width = width
        self.__boardX = boardX
        self.__boardY = boardY

    # Function ot draw chessboard
    def __drawRectangle(self):
        self.__tr.pendown()
        self.__tr.color("Black")
        self.__tr.begin_fill()
        self.__tr.forward(self.__width / 8)
        self.__tr.left(90)
        self.__tr.forward(self.__height / 8)
        self.__tr.left(90)
        self.__tr.forward(self.__width / 8)
        self.__tr.left(90)
        self.__tr.forward(self.__height / 8)
        self.__tr.left(90)
        self.__tr.end_fill()

    # Function to draw all rectangles on the chessboard
    def __drawAllRectangles(self):
        self.__tr.speed(8)
        evenRectSpace = 0
        oddRectSpace = self.__height / 8
        # Draw first rows of rectangles
        for j in range(4):
            evenRectSpace += (self.__height / 8) * 2
            for i in range(4):
                i = (self.__width / 8) * 2
                Chessboard.__drawRectangle(self)
                self.__tr.penup()
                self.__tr.forward(i)
                self.__tr.pendown()
            self.__tr.penup()
            self.__tr.goto(self.__boardX, self.__boardY + evenRectSpace)
            self.__tr.pendown()

        # Set up for new loop, draw rest of chessboard squares
        self.__tr.penup()
        self.__tr.goto(self.__boardX + (self.__width / 8), self.__boardY + (self.__height / 8))
        self.__tr.pendown()

        # Draw next rows of rectangles
        for a in range(4):
            oddRectSpace += (self.__height / 8) * 2
            for b in range(4):
                b = (self.__width / 8) * 2
                Chessboard.__drawRectangle(self)
                self.__tr.penup()
                self.__tr.forward(b)
                self.__tr.pendown()
            self.__tr.penup()
            self.__tr.goto(self.__boardX + (self.__width / 8), self.__boardY + oddRectSpace)
            self.__tr.pendown()

        self.__tr.hideturtle()

    # Function to draw the chessboard
    def draw(self):

        # Draw border
        self.__tr.color("Green")
        self.__tr.penup()
        self.__tr.goto(self.__boardX, self.__boardY)
        self.__tr.pendown()
        self.__tr.forward(self.__width)
        self.__tr.left(90)
        self.__tr.forward(self.__height)
        self.__tr.left(90)
        self.__tr.forward(self.__width)
        self.__tr.left(90)
        self.__tr.forward(self.__height)
        self.__tr.left(90)
        self.__tr.color("Black")

        # Draw rectangles
        self.__tr.penup()
        self.__tr.goto(self.__boardX, self.__boardY)
        self.__tr.pendown()
        Chessboard.__drawAllRectangles(self)
        # Done
        self.__tr.hideturtle()
