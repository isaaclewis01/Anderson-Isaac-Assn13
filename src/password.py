# Isaac Anderson
# CS 1400-1
# Assn 13 Task 2
# Program to check a password.


class Password:
    def __init__(self, password, message=""):
        self.__password = password
        self.__message = message

    def setPassword(self, password):
        self.__password = password

    def __checkOne(self):
        if self.__password.isalnum():
            return True
        else:
            self.__message += "\nPassword can only contain numbers and letters."
            return False

    def __checkTwo(self):
        if self.__password.__len__() == self.__password.find("123") + 3 and self.__password.__contains__("123"):
            self.__message += "\nPassword cannot end in \'123\'."
            return False
        else:
            return True

    def __checkThree(self):
        if self.__password.__len__() > 7:
            return True
        else:
            self.__message += "\nPassword must be longer than 8 characters."
            return False

    def __checkFour(self):
        count = 0
        for i in range(self.__password.__len__()):
            if self.__password.__getitem__(i).isdigit():
                count += 1
        if count >= 2:
            return True
        else:
            self.__message += "\nPassword must have more than 2 digits."
            return False

    def __checkFive(self):
        if self.__password.__contains__("password"):
            self.__message += "\nPassword cannot contain the string \"password\""
            return False
        else:
            return True

    def isValid(self):
        if self.__checkOne() and self.__checkTwo() and self.__checkThree() and self.__checkFour() and self.__checkFive():
            return "Good password."
        else:
            self.__message = ""
            self.__checkOne()
            self.__checkTwo()
            self.__checkThree()
            self.__checkFour()
            self.__checkFive()
            return self.getErrorMessage()

    def getErrorMessage(self):
        return self.__message
