# Object-Oriented Chessboard Structure and Password Checker Assignment

## NOTE
This was an assignment done for CS1400 at Utah State University, some code from the project was done by Chad Mano as a basic for the assignment, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code.

## Manual
This program has two parts. The first is a refactor of the Anderson-Isaac-Assn10 project, using object-oriented programming to change how the code is organized. The second part is a password checker that "verifies" you have a good password, like one you'd see when making an acount with someone. When running the files assn13-task1 and assn13-task2, there will be clear instructions on how to use the program.
